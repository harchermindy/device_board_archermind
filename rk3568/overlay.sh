#!/bin/bash

# Copyright (c) 2021 HiHope Open Source Organization .
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


echo "**************************"
echo ${device_name}
echo ${source_root_dir}

echo ${source_root_dir}/vendor/archermind/board/${device_name}/overlay/display_manager_config.xml
echo ${source_root_dir}/foundation/window/window_manager/resources/config/rk3568/display_manager_config.xml
cp ${source_root_dir}/vendor/archermind/board/${device_name}/overlay/display_manager_config.xml ${source_root_dir}/foundation/window/window_manager/resources/config/rk3568/display_manager_config.xml

echo ${source_root_dir}/vendor/archermind/board/${device_name}/overlay/power_mode_config.xml
echo ${source_root_dir}/base/powermgr/power_manager/services/native/profile/power_mode_config.xml
cp ${source_root_dir}/vendor/archermind/board/${device_name}/overlay/power_mode_config.xml ${source_root_dir}/base/powermgr/power_manager/services/native/profile/power_mode_config.xml


echo ${source_root_dir}/applications/standard/system_hap/${device_name}/
echo ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Calc_Demo.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/CallUI.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Camera.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/CertificateManager.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Clock_Demo.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Contacts.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/FilePicker.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/kikaInput.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Launcher.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Launcher_Settings.hap ${source_root_dir}/applications/standard/hap/

cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Mms.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/MobileDataSettings.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Music_Demo.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Note.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Photos.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/ScreenShot.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Settings.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Settings_FaceAuth.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SettingsData.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/Shopping_Demo.hap ${source_root_dir}/applications/standard/hap/

cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI-DropdownPanel.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI-NavigationBar.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI-NotificationManagement.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI-ScreenLock.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI-StatusBar.hap ${source_root_dir}/applications/standard/hap/
cp ${source_root_dir}/applications/standard/system_hap/${device_name}/SystemUI-VolumePanel.hap ${source_root_dir}/applications/standard/hap/
